#include <mpi.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <chrono>
#include <string>
#include <numeric>
#include <cassert>

double buffersend(const std::string& method, const double* m, size_t n);

int main() {
  int mpisize, mpirank;
  MPI_Init(nullptr, nullptr);
  MPI_Comm_size(MPI_COMM_WORLD, &mpisize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
  if (mpirank == 0)
    std::cout << mpisize << " MPI ranks" << std::endl;

  constexpr size_t n = 10000;
  std::vector<double> m(n * n);
  constexpr double angle = 1.2345;
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++)
      m[i * n + j] = 0;
    if (i % 2) {
      m[(i - 1) * n + i - 1] = m[i * n + i] = cos(angle);
      m[i * n + i - 1] = -(m[(i - 1) * n + i] = sin(angle));
    }
  }

  for (const auto& method : std::vector<std::string>{"buffer+send", "ddt"}) {
    auto t = buffersend(method, m.data(), n);
    if (mpirank == 0)
      std::cout << "time for " << method << "(" << n << "*" << n << ")=" << t << std::endl;
  }

  MPI_Finalize();
  return 0;
}

double buffersend(const std::string& method, const double* m, size_t n) {
  int mpisize, mpirank;
  MPI_Comm_size(MPI_COMM_WORLD, &mpisize);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
  std::vector<double> mt(n * n);
  MPI_Request request;
  MPI_Status stat;
  int partner = mpisize - mpirank - 1;
  auto begin = std::chrono::steady_clock::now();

  if (method == "buffer+send") {
    double buffer[n];
    for (size_t i = 0; i < n; i++) {
      for (size_t j = 0; j < n; j++)
        buffer[j] = m[j * n + i];
      MPI_Isend(buffer, n, MPI_DOUBLE, partner, i, MPI_COMM_WORLD, &request);
      MPI_Recv(&mt[i * n], n, MPI_DOUBLE, partner, i, MPI_COMM_WORLD, &stat);
      MPI_Wait(&request, &stat);
    }
  }

  if (method == "ddt") {
    MPI_Datatype buffer_t;
    MPI_Type_vector(n, 1, n, MPI_DOUBLE, &buffer_t);
    MPI_Type_commit(&buffer_t);
    for (size_t i = 0; i < n; i++) {
      MPI_Isend(&m[i], 1, buffer_t, partner, i, MPI_COMM_WORLD, &request);
      MPI_Recv(&mt[i * n], n, MPI_DOUBLE, partner, i, MPI_COMM_WORLD, &stat);
      MPI_Wait(&request, &stat);
    }
  }
  auto t =
      (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - begin).count()) * 1e-9;
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      assert (std::abs(mt[i * n + j] - m[j * n + i]) < 1e-14);
    }
  }
  return t;
}
